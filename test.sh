#! /bin/bash

# Global variables
ITS=insert_then_sort
SI=sorted_insertion

# Temporary files to store on disk test's output
SI_TMP=$(mktemp)
ITS_TMP=$(mktemp)

function cleanup_binaries {
	rm -f $ITS
	rm -f $SI
    rm -f $SI_TMP
    rm -f $ITS_TMP
	echo
}

function compile_binaries {
	cc main.c -o $ITS -L libft -I libft/incs -lft \
		&& cc main2.c -o $SI -L libft -I libft/incs -lft \
		|| exit 1
}

function make_test {
	list_size=$1

    ./$SI $list_size > $SI_TMP & \
        ./$ITS $list_size > $ITS_TMP

    wait

    echo "$SI($list_size) took $(cat $SI_TMP) cycles"
    echo "$ITS($list_size) took $(cat $ITS_TMP) cycles"

	wait
}

function loop_test {
	while read -p "Give the size of the list that you want to test ( or CTRL-D to quit ): " SIZE
	do
		make_test $SIZE
		echo
	done
}

function main {
	trap "cleanup_binaries && exit 0" INT
	compile_binaries
	loop_test
	cleanup_binaries
}
main
