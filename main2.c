
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>
#include <stdio.h>

#include "libft.h"

typedef	struct	s_liste
{
	char			*name;
	struct	s_liste	*next;
	struct	s_liste	*previous;
}				t_liste;

size_t list_size(t_liste *l)
{
	size_t		length = 0;

	while (l != NULL)
	{
		length++;
		l = l->next;
	}
	return length;
}

t_liste     *new_node(char *name)
{
	t_liste *new;

	new =(t_liste*)malloc(sizeof(t_liste));
	if (!new)
		return (NULL);
	new->name = ft_strdup(name);
	new->next = NULL;
	new->previous = NULL;
	return(new);
}

int		insert_node(t_liste **head, char *name)
{
	t_liste		*node;
	t_liste		*current;

	current = *head;
	node = new_node(name);
	if (*head == NULL)
	{
		*head = node;
		return (0);
	}

	if (ft_strcmp(current->name, node->name) > 0)
	{
		node->next = current;
		current->previous = node;
		current = node;
		*head = current;
	}
	else
	{
		while (current->next != NULL && ft_strcmp(current->next->name,
					node->name) < 0)
			current = current->next;
		node->next = current->next;
		if (current->next != NULL)
			node->next->previous = node;
		current->next = node;
	}
	return (0);
}

int		main(int argc, char **argv)
{
	char		data[16] = {0};
	struct s_liste	*l = NULL;
	clock_t insertion_start, insertion_end;

	assert(argc > 1);
	insertion_start = clock();
	for (size_t i = 0; i < atoi(argv[1]); i++)
	{
		sprintf(data, "%d", (int)i - 50);
		insert_node(&l, data);
	}
	insertion_end = clock();

	printf("%lu\n", insertion_end - insertion_start);
	assert(list_size(l) == (size_t)atoi(argv[1]));

	return (1);
}
