
## Benchmark

This project is made to test two way of have a sorted linked list.

The first way make sorted insertion node. Every time a node is added to the
list, it is inserted at his correct place
( named: `sorted_insertion` )

The second way insert a node on the fastest way possible with this list's API,
and the sort the list once the list has it's final size.
( named: `insert_then_sort` )


#### Some results

Tests made with a Intel(R) Core(TM) i5-5###U CPU @ 2.00GHz on a linux mint distro
```
Give the size of the list that you want to test ( or CTRL-D to quit ): 1
sorted_insertion(1) took 78 cycles
insert_then_sort(1) took 66 cycles

Give the size of the list that you want to test ( or CTRL-D to quit ): 10
sorted_insertion(10) took 87 cycles
insert_then_sort(10) took 69 cycles

Give the size of the list that you want to test ( or CTRL-D to quit ): 100
sorted_insertion(100) took 163 cycles
insert_then_sort(100) took 124 cycles

Give the size of the list that you want to test ( or CTRL-D to quit ): 1000
sorted_insertion(1000) took 3792 cycles
insert_then_sort(1000) took 751 cycles

Give the size of the list that you want to test ( or CTRL-D to quit ): 10000
sorted_insertion(10000) took 322453 cycles
insert_then_sort(10000) took 7556 cycles
```


#### How to run these tests on my machine ?

compile the libft with the command `make -C libft`, run `./test.sh`

The script will ask you the size of the list you want to have, then run tests
and then reask. Quit with ctrl-C or ctrl-D
