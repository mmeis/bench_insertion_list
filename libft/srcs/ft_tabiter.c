/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 18:22:53 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 18:28:46 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void		ft_tabiter(char **tab, void (*callback)())
{
	size_t		i;

	i = 0;
	while (tab[i] != NULL)
	{
		callback(tab[i]);
		i++;
	}
}
