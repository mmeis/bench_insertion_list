/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:59:59 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/11 11:02:35 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <stdlib.h>

static inline void	membigcpy(uint64_t *dest, const uint64_t *src, size_t n)
{
	size_t		i;

	i = 0;
	while (i < n)
	{
		dest[i] = src[i];
		i++;
	}
}

static inline void	memsmallcpy(uint8_t *dest, const uint8_t *src, size_t n)
{
	size_t		i;

	i = 0;
	while (i < n)
	{
		dest[i] = src[i];
		i++;
	}
}

void				*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t		big_size;
	size_t		small_size;

	big_size = n / sizeof(uint64_t);
	small_size = n % sizeof(uint64_t);
	membigcpy(dest, src, big_size);
	memsmallcpy(dest + (n - small_size), src + (n - small_size), small_size);
	return (dest);
}
