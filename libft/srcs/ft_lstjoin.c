/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 15:49:15 by mmeisson          #+#    #+#             */
/*   Updated: 2018/11/27 16:08:15 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		*make_join(
		struct s_list *iter,
		char join,
		size_t final_length,
		int has_join)
{
	size_t	length;
	char	*joined;

	length = 0;
	joined = malloc(final_length + 1);
	if (joined)
	{
		while (iter)
		{
			ft_memcpy(joined + length, iter->content, iter->content_size - 1);
			length += iter->content_size - 1;
			joined[length] = join;
			length += has_join;
			iter = iter->next;
		}
		joined[length] = 0;
	}
	return (joined);
}

char			*ft_lstjoin(struct s_list *strings, char join)
{
	int				has_join;
	struct s_list	*iter;
	size_t			length;

	has_join = join != 0;
	iter = strings;
	length = 0;
	while (iter != NULL)
	{
		length += (iter->content_size - 1) + has_join;
		iter = iter->next;
	}
	return (make_join(strings, join, length, has_join));
}
