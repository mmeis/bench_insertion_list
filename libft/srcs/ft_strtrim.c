/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:09:41 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/11 11:00:55 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static inline int	to_trim(int c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

char				*ft_strtrim(const char *s)
{
	size_t	size;

	while (*s != 0 && to_trim(*s))
		s++;
	size = ft_strlen(s);
	if (size == 0)
	{
		return (ft_strnew(0));
	}
	size--;
	while (size > 0 && to_trim(s[size]))
		size--;
	return (ft_strndup(s, size + 1));
}
