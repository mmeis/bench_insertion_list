/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_umin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 16:06:07 by mmeisson          #+#    #+#             */
/*   Updated: 2018/11/27 16:06:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_umin(
	unsigned int a,
	unsigned int b
)
{
	return (a > b ? b : a);
}
