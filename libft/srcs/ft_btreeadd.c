/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreeadd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 13:44:15 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:06:25 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_btreeadd(t_btree **abtree, t_btree *new,
		int (*cmp)(const void*, const void*, size_t))
{
	int		i;

	if (!*abtree)
		*abtree = new;
	else
	{
		i = cmp((*abtree)->content, new->content, (*abtree)->content_size);
		if (i > 0)
			ft_btreeadd(&(*abtree)->left, new, cmp);
		else if (!i)
		{
			if (new->content_size <= (*abtree)->content_size)
				ft_btreeadd(&(*abtree)->left, new, cmp);
			else
				ft_btreeadd(&(*abtree)->right, new, cmp);
		}
		else
			ft_btreeadd(&(*abtree)->right, new, cmp);
	}
}
