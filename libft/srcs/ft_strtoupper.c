/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoupper.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 14:31:36 by mmeisson          #+#    #+#             */
/*   Updated: 2017/09/25 14:33:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		upper(char *c)
{
	*c = ft_toupper(*c);
}

char			*ft_strtoupper(char *str)
{
	ft_striter(str, upper);
	return (str);
}
