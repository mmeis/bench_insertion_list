/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstin.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/04 17:39:16 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/04 17:39:18 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_lstin(void *data, size_t data_size, const struct s_list *lst)
{
	while (lst)
	{
		if (data_size == lst->content_size
			&& ft_memcmp(data, lst->content, data_size) == 0)
		{
			return (1);
		}
		lst = lst->next;
	}
	return (0);
}
