/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:00:07 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/11 11:02:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdint.h>
#include "libft.h"

static inline void	membigcpy(uint64_t *dest, const uint64_t *src, size_t n)
{
	while (n > 0)
	{
		n--;
		dest[n] = src[n];
	}
}

static inline void	memsmallcpy(uint8_t *dest, const uint8_t *src, size_t n)
{
	while (n > 0)
	{
		n--;
		dest[n] = src[n];
	}
}

void				*ft_memmove(void *dest, const void *src, size_t n)
{
	size_t		big_size;
	size_t		small_size;

	if (dest < src)
		ft_memcpy(dest, src, n);
	else if (dest > src)
	{
		big_size = n / sizeof(uint64_t);
		small_size = n % sizeof(uint64_t);
		memsmallcpy(
			dest + (n - small_size),
			src + (n - small_size),
			small_size);
		membigcpy(dest, src, big_size);
	}
	return (dest);
}
