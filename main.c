
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>
#include <stdio.h>

#include "libft.h"

int		main(int argc, char **argv)
{
	char		data[16] = {0};
	struct s_list	*l = NULL;
	clock_t insertion_start, merge_end;

	assert(argc > 1);

	insertion_start = clock();
	for (size_t i = 0; i < atoi(argv[1]); i++)
	{
		sprintf(data, "%zu", i);
		struct s_list *node = ft_lstnew(data, sizeof(data));

		assert(node != NULL);
		ft_lstadd(&l, node);
	}

	l = merge_list(l, strcmp);
	merge_end = clock();

	printf("%lu\n", merge_end - insertion_start);
	return (1);
}
